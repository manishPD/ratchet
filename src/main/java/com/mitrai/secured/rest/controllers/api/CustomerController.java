package com.mitrai.secured.rest.controllers.api;

import com.mitrai.secured.rest.controllers.dto.CustomDto;
import com.mitrai.secured.rest.controllers.transformer.CustomerTransformer;
import com.mitrai.secured.rest.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;

@RequestMapping("/api")
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    /**
     * This API will return a CustomDTO object containing list of all active customers
     * success: will indicate the success status of the operation as a boolean
     * o: will contain list of active customers
     * message: will contain any specific display message. if success message will remain null
     * @return
     */
    @RequestMapping(value = "/allCustomers", method = RequestMethod.GET)
    public CustomDto getAllCustomers() {
        LOGGER.info("Fetching all customers...");
        CustomDto customDto = CustomDto.getInstance();
        try {
            customDto.setO(CustomerTransformer.getCustomerTransformer()
                    .getTransformedCustomerSet(customerService.getAllActiveCustomers()));
            customDto.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            LOGGER.error("Error while fetching all customers ", e);
            customDto.setSuccess(Boolean.FALSE);
            customDto.setMessage("error.fetching.all.customers");
        } finally {
            return customDto;
        }
    }

    /**
     * This API will return a CustomDTO object containing a single instance a customer as per the customer code
     * success: will indicate the success status of the operation as a boolean
     * o: will contain list of active customers
     * message: will contain any specific display message. if success message will remain null
     * @param customerCode : will inject as a GET (String customer code)
     * @return
     */
    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public CustomDto getCustomerByCode(@RequestParam("code") String customerCode) {
        LOGGER.info(MessageFormat.format("Fetching customer for code {0}...", customerCode));
        CustomDto customDto = CustomDto.getInstance();
        try {
            customDto.setO(CustomerTransformer.getCustomerTransformer()
                    .getTransformedCustomer(customerService.getCustomerByCode(customerCode)));
            customDto.setSuccess(Boolean.TRUE);
        } catch (Exception e) {
            customDto.setSuccess(Boolean.FALSE);
            customDto.setMessage("error.fetching.customer.by.id");
        } finally {
            return customDto;
        }
    }

}
