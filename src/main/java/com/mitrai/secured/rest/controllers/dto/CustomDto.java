package com.mitrai.secured.rest.controllers.dto;

public class CustomDto {

    private CustomDto() {

    }

    private static CustomDto customDto;

    public static CustomDto getInstance() {
        if (customDto == null) {
            synchronized (CustomDto.class) {
                if (customDto == null) {
                    customDto = new CustomDto();
                }
            }
        }
        return customDto;
    }

    private Object o;
    private Boolean success;
    private String message;

    public Object getO() {
        return o;
    }

    public void setO(Object o) {
        this.o = o;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
