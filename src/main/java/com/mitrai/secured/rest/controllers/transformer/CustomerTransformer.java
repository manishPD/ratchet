package com.mitrai.secured.rest.controllers.transformer;

import com.mitrai.secured.rest.controllers.dto.CustomersDto;
import com.mitrai.secured.rest.models.Customer;

import java.util.HashSet;
import java.util.Set;


public class CustomerTransformer {

    private CustomerTransformer() {

    }

    private static CustomerTransformer customerTransformer;

    public static CustomerTransformer getCustomerTransformer() {
        if (customerTransformer == null) {
            synchronized (CustomerTransformer.class) {
                if (customerTransformer == null) {
                    customerTransformer = new CustomerTransformer();
                }
            }
        }
        return customerTransformer;
    }

    public CustomersDto getTransformedCustomer(Customer customer) {
        return new CustomersDto(customer.getCustomerCode(), customer.getFirstName(), customer.getLastName(), customer.getAddressLine1(),
                customer.getAddressLine2(), customer.getEmail(), customer.getPrimaryContact(), customer.getSecondaryContact(),
                customer.getStateCode(), customer.getActive());
    }

    public Set<CustomersDto> getTransformedCustomerSet(Set<Customer> customers) {
        Set<CustomersDto> dto = new HashSet<>();
        for (Customer c : customers) {
            dto.add(getTransformedCustomer(c));
        }
        return dto;
    }

}
