package com.mitrai.secured.rest.services;


import com.mitrai.secured.rest.models.Customer;
import com.mitrai.secured.rest.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Set<Customer> getAllActiveCustomers() {
        return customerRepository.getAllActiveCustomers();
    }

    public Customer getCustomerByCode(String customerCode) {
        return customerRepository.findByCustomerCode(customerCode);
    }

}
