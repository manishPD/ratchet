package com.mitrai.secured.rest.services;

import com.mitrai.secured.rest.models.CustomUserDetails;
import com.mitrai.secured.rest.models.Users;
import com.mitrai.secured.rest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Users> users = userRepository.findByEmail(email);
        users.orElseThrow(() -> new UsernameNotFoundException("Cannot find the user by given name"));
        return users.map(CustomUserDetails::new).get();
    }
}
