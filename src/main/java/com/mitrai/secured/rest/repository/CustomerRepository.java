package com.mitrai.secured.rest.repository;

import com.mitrai.secured.rest.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface CustomerRepository extends JpaRepository <Customer, String> {

    @Query("select c from Customer c where c.active = 1 order by c.firstName DESC")
    Set<Customer> getAllActiveCustomers();

    Customer findByCustomerCode(String customerCode);

}
