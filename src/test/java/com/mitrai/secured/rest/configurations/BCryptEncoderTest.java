package com.mitrai.secured.rest.configurations;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class BCryptEncoderTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BCryptEncoderTest.class);

    @Test
    public void encryptGivenPassword() {
        String password = "mit123";
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(12));
        LOGGER.info("Encrypted password : " + hashedPassword);
    }

}
